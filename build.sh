#!/usr/bin/env bash

set -e

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

pushd src/MSM

make insertcopyright

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

make clean
make
make install
popd

mkdir -p $FSLDIR/etc/MSM/
cp -r config/* $FSLDIR/etc/MSM/
